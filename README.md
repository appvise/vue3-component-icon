# @appvise/vue3-component-icon

Mapped icon component, allowing easy switchin between icon sets

## Usage

To select a specific icon set instead of the default

```vue
<style>
@import url('https://fonts.googleapis.com/css2?family=Material+Icons+Round');
</style>

<script>
// In your entrypoint (e.g. root.vue, app.vue, etc)
import { provide } from 'vue';

export default {
  setup() {
    provide('icon-set', 'material-rounded');
  }
}
</script>
```

Now if you want to actually use the icon:

```vue
<template>
  <icon>sidenav-left-open</icon>
</template>

<style>
.icon {
  /* allows custom css as well */
}
</style>

<script>
import Icon from '@appvise/vue3-component-icon';

export default {
  components: {Icon}
}
</script>
```

## Mapping

| Name               | Material Icons | Feather      |
| ------------------ | -------------- | ------------ |
| logout             | logout         | log-out      |
| modal-close        | close          | x            |
| sidenav-left-close | chevron_left   | chevron-left |
| sidenav-left-open  | menu           | menu         |

## License

Copyright © 2021 App-vise B.V.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const vuePlugin = require("esbuild-plugin-vue3");
const esbuild  = require("esbuild");
const rimraf   = require("rimraf");
const fs       = require('fs');

const destDir = __dirname + "/dist";

(async () => {
  await new Promise(r => rimraf(__dirname + "/dist", r));

  // Build vue
  await esbuild.build({
    plugins: [vuePlugin()],
    entryPoints: [__dirname + "/lib/index.vue"],
    bundle: true,
    outfile: __dirname + "/dist/index.js"
  });
})();
